# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring
from typing import Callable
from typing import Dict

import pytest

from erdfuchs_codegen.gen_models import openapi_type_to_python_type
from erdfuchs_codegen.gen_models import properties_to_members
from erdfuchs_codegen.gen_models import optional_members
from erdfuchs_codegen.gen_models import required_members
from erdfuchs_codegen.gen_models import dependencies


@pytest.mark.parametrize("given, expected", [
    ({
        "type": "string",
        "default": "de",
    }, "str"),
    ({
        "type": "integer",
        "format": "int32",
        "default": 1,
    }, "int"),
    ({
        "type": "number",
        "format": "double",
    }, "float"),
    ({
        "type": "boolean",
        "default": False,
    }, "bool"),
    ({
        "type": "array",
        "items": {
            "type": "integer",
            "format": "int32",
        }
    }, "List[int]"),
    ({
        "$ref": "#/components/schemas/RequiredRegionType",
    }, "RequiredRegionType")
])
def test_openapi_type_to_python_type(given: Dict, expected: str) -> None:
    assert openapi_type_to_python_type(given) == expected


def test_properties_to_members() -> None:
    given = {
        "type": {
            "type": "string",
        },
        "count": {
            "type": "integer",
            "format": "int32",
        },
        "from": {
            # This is included to test that member names that are also
            # python keywords are properly mangled
            "type": "integer",
        },
    }
    expected = {
        "type": "str",
        "count": "int",
        "_from": "int",
    }
    assert properties_to_members(given) == expected


@pytest.mark.parametrize("func, expected", [
    (optional_members, {"value": "int"}),
    (required_members, {"key": "str"}),
])
def test_optional_required_members(
        func: Callable[[Dict], Dict[str, str]],
        expected: Dict[str, str],
) -> None:
    given = {
        "required": ["key"],
        "type": "object",
        "properties": {
            "key": {
                "type": "string"
            },
            "value": {
                "type": "integer"
            }
        }
    }

    assert func(given) == expected


@pytest.mark.parametrize("func, expected", [
    (optional_members, {
        "personType": "str",
        "personCount": "int",
    }),
    (required_members, {}),
])
def test_optional_required_members_without_required_attribute(
        func: Callable[[Dict], Dict[str, str]],
        expected: Dict[str, str],
) -> None:
    given = {
        "type": "object",
        "properties": {
            "personType": {
                "type": "string",
            },
            "personCount": {
                "type": "integer",
            },
        }
    }

    assert func(given) == expected


@pytest.mark.parametrize("func, expected", [
    (optional_members, {
        "filterType": "str",
    }),
    (required_members, {}),
])
def test_members_with_default_are_never_required(
        func: Callable[[Dict], Dict[str, str]],
        expected: Dict[str, str],
) -> None:
    given = {
        "required": ["filterType"],
        "type": "object",
        "properties": {
            "filterType": {
                "type": "string",
                "default": "NO_FILTER",
            },
        }
    }

    assert func(given) == expected


def test_dependencies() -> None:
    given = {
        "errorText": {
            "type": "string",
        },
        "courseElements": {
            "type": "array",
            "items": {
                "$ref": "#/components/schemas/CourseElement",
            },
        },
        "attributes": {
            "type": "array",
            "items": {
                "$ref": "#/components/schemas/Attribute",
            },
        },
    }

    expected = {"CourseElement", "Attribute"}

    assert dependencies(given) == expected
