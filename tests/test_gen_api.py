# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring
from erdfuchs_codegen.gen_api import endpoint_name
from erdfuchs_codegen.gen_api import request_class
from erdfuchs_codegen.gen_api import response_class

given = {
    "post": {
        "tags": [
            "init-request-controller",
        ],
        "operationId": "init",
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/InitRequest",
                    }
                }
            },
            "required": True,
        },
        "responses": {
            "200": {
                "description": "OK",
                "content": {
                    "*/*": {
                        "schema": {
                            "$ref": "#/components/schemas/InitResponse",
                        }
                    }
                }
            }
        }
    }
}


def test_endpoint_name() -> None:
    expected = "init"

    assert endpoint_name(given) == expected


def test_request_class() -> None:
    expected = "InitRequest"

    assert request_class(given) == expected


def test_response_class() -> None:
    expected = "InitResponse"

    assert response_class(given) == expected
