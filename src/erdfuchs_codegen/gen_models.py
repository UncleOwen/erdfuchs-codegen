"""
Generate api files
"""


import json
from pathlib import Path
from typing import Dict
from typing import Set
from typing import Optional
from keyword import kwlist

from jinja2 import Template


def gen_models(output_path: Path, openapi_description: Dict) -> None:
    """
    Generate lots of model files in output_path
    """
    output_path.mkdir(exist_ok=True, parents=True)

    with open(output_path / "__init__.py", "w", encoding="ascii"):
        pass

    for model_name, model_description \
            in openapi_description["components"]["schemas"].items():
        try:
            assert all(letter.isalpha for letter in model_name)
            gen_model(
                output_path / f"{model_name}.py",
                model_name,
                model_description
            )
        except Exception as err:  # pylint: disable=broad-except
            print(f"Problem generation python code for {model_name}")
            print(err)


def gen_model(
        output_path: Path,
        model_name: str,
        model_description: Dict
) -> None:
    """
    Write one model to output_path.
    """
    template = Template(
        """
from dataclasses import dataclass
from typing import List
from typing import Optional
import marshmallow_dataclass

{% for dep in dependencies %}
from .{{ dep }} import {{ dep }}
{% endfor %}


@dataclass()
class {{ model_name }}:
{% for member, type_description in req_members.items() %}
    {{ member }}: {{ type_description }}
{% endfor %}
{% for member, type_description in opt_members.items() %}
    {{ member }}: Optional[{{ type_description }}] = None
{% endfor %}


schema = marshmallow_dataclass.class_schema({{ model_name }})()
""",
        trim_blocks=True,
        keep_trailing_newline=True,
    )

    with open(output_path, "w", encoding='utf-8') as out_f:
        out_f.write(template.render(
            model_name=model_name,
            req_members=required_members(model_description),
            opt_members=optional_members(model_description),
            dependencies=dependencies(model_description["properties"])
        ))


def openapi_type_to_python_type(type_description: Dict) -> str:
    """
    Convert an openAPI type description to a python type name
    """
    if "type" in type_description:
        if type_description["type"] == "string":
            return "str"
        if type_description["type"] == "integer":
            return "int"
        if type_description["type"] == "number":
            return "float"
        if type_description["type"] == "boolean":
            return "bool"
        if type_description["type"] == "array":
            return (
                    "List["
                    + openapi_type_to_python_type(type_description['items'])
                    + "]"
            )
    if "$ref" in type_description:
        prefix = "#/components/schemas/"
        if type_description["$ref"].startswith(prefix):
            return type_description["$ref"][len(prefix):]
    raise NotImplementedError("Unknown OpenAPI type description: " +
                              json.dumps(type_description))


def mangle_keywords(name: str) -> str:
    """
    returns name unless it is a python keyword,
    in which case _name is returned.
    """
    return name if name not in kwlist else f"_{name}"


def properties_to_members(properties: Dict) -> Dict[str, str]:
    """
    convert an openAPI properties Hash to a Dictionary {member -> type}
    """
    return {
        mangle_keywords(member): openapi_type_to_python_type(type_description)
        for member, type_description
        in properties.items()
    }


def optional_members(model_description: Dict) -> Dict[str, str]:
    """
    takes a model description, returns a Dictionary {member -> type} containing
    all optional members
    """
    if "required" in model_description:
        return {
            member: python_type
            for member, python_type
            in properties_to_members(model_description["properties"]).items()
            if (
                member not in model_description["required"]
                or "default" in model_description["properties"][member]
            )
        }
    return properties_to_members(model_description["properties"])


def required_members(model_description: Dict) -> Dict[str, str]:
    """
    takes a model description, returns a Dictionary {member -> type} containing
    all non-optional members
    """
    if "required" in model_description:
        return {
            member: python_type
            for member, python_type
            in properties_to_members(model_description["properties"]).items()
            if (
                member in model_description["required"]
                and "default" not in model_description["properties"][member]
            )
        }
    return {}


def dependencies(properties: Dict) -> Set[str]:
    """
    which other model files do we need to import?
    """
    def type_to_dependencies(type_description: Dict) -> Optional[str]:
        """
        dependencies generated by one type description
        """
        if "type" in type_description and type_description["type"] == "array":
            return type_to_dependencies(type_description['items'])
        if "$ref" in type_description:
            prefix = "#/components/schemas/"
            if type_description["$ref"].startswith(prefix):
                return type_description["$ref"][len(prefix):]
        return None

    result = set()
    for type_description in properties.values():
        dep = type_to_dependencies(type_description)
        if dep:
            result.add(dep)
    return result
