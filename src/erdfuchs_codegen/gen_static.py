"""
Generate static files.
"""

# F I X M E: Convert all this to resources
from pathlib import Path


def gen_static(output_path: Path) -> None:
    """
    Generate static files.
    """
    with open(output_path / "tox.ini", "w", encoding="ascii") as out_f:
        out_f.write("""
[tox]
envlist = py3{7,8,9,10}

[testenv]
deps =
    mypy
    pytest
commands =
    mypy src/ tests/
    pytest tests {posargs}
""".lstrip())

    with open(output_path / "setup.py", "w", encoding="ascii") as out_f:
        out_f.write("""
from setuptools import setup

setup()
""".lstrip())

    with open(output_path / "setup.cfg", "w", encoding="utf-8") as out_f:
        out_f.write("""
[metadata]
name = erdfuchs-generated
description = API files for geofox GTI api (generated files)
long_description = file: README.md
long_description_content_type = text/markdown;charset=UTF-8;variant=CommonMark
author = Sören Glimm
author_email = git@uncleowen.de
license = Apache
classifiers =
    Programming Language :: Python :: 3
    License :: OSI Approved :: Apache Software License
    Operating System :: OS Independent
    Development Status :: 3 - Alpha

[options]
package_dir =
    =src
packages = find_namespace:
install_requires =
    marshmallow-dataclass

[options.packages.find]
where=src

[options.package_data]
erdfuchs.generated =
    py.typed
""".lstrip())

    with open(output_path / "pyproject.toml", "w", encoding="ascii") as out_f:
        out_f.write("""
[build-system]
requires = [
  "setuptools>=43.0.0",
  "wheel",
]
build-backend = "setuptools.build_meta"
""".lstrip())

    test_path = output_path / "tests"
    test_path.mkdir(exist_ok=True)
    with open(test_path / "test_imports.py", "w", encoding="ascii") as out_f:
        out_f.write("""
# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring


def test_imports() -> None:
    # Test that importing the api file works
    # If that works, we at least build a valid python package.
    from erdfuchs.generated.api import Api
""".lstrip())
