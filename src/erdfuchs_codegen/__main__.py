"""
main entry point.
"""
import json
from pathlib import Path

from .gen_models import gen_models
from .gen_api import gen_api
from .gen_static import gen_static
from .gen_setup_cfg import gen_setup_cfg


def main() -> None:
    """
    main entry point
    """
    input_path = Path(".") / "data" / "api.json"
    with open(input_path, "rt", encoding='ascii') as in_f:
        openapi_description = json.load(in_f)

    output_path = Path(".") / "output"
    output_path.mkdir(exist_ok=True)
    gen_static(output_path)
    gen_setup_cfg(output_path / "setup.cfg")
    gen_models(
        output_path / "src" / "erdfuchs" / "generated" / "models",
        openapi_description,
    )
    gen_api(
        output_path / "src" / "erdfuchs" / "generated",
        openapi_description,
    )


if __name__ == "__main__":
    main()
