"""
generate the file with the api calls
"""


from dataclasses import dataclass
from pathlib import Path
from typing import Dict
from typing import List

from jinja2 import Template


def gen_api(output_path: Path, openapi_description: Dict) -> None:
    """
    generate the file with the api calls
    """
    template = Template(
        """
from typing import Callable
from typing import Dict

from marshmallow_dataclass import class_schema

{% for endpoint in endpoints %}
from .models.{{ endpoint.request_class }} import {{ endpoint.request_class }}
from .models.{{ endpoint.response_class }} import {{ endpoint.response_class }}
{% endfor %}


class Api:
    def _request_func(self, endpoint: str, args: Dict) -> Dict:
        raise NotImplementedError()

{% for endpoint in endpoints %}
    def {{ endpoint.name }}(
            self,
            req: {{ endpoint.request_class }}
    ) -> {{ endpoint.response_class }}:

        result = self._request_func(
            "{{ endpoint.path }}",
            class_schema({{ endpoint.request_class }})().dump(req),
        )
        return class_schema({{ endpoint.response_class }})().load(result)

{% endfor %}
""",  # noqa: E501
        trim_blocks=True,
        keep_trailing_newline=True,
    )

    output_path.mkdir(exist_ok=True, parents=True)

    with open(output_path / "__init__.py", "w", encoding="ascii"):
        pass

    with open(output_path / "py.typed", "w", encoding="ascii"):
        pass

    with open(output_path / "api.py", "w", encoding="ascii") as out_f:
        out_f.write(template.render(
            endpoints=endpoints(openapi_description["paths"]),
        ))


@dataclass()
class _Endpoint:
    name: str
    path: str
    request_class: str
    response_class: str


def endpoints(paths_description: Dict) -> List[_Endpoint]:
    """
    returns a list of _Endpoint objects from the paths part of the openAPI file
    """
    return [
        _Endpoint(
            endpoint_name(endpoint_description),
            path,
            request_class(endpoint_description),
            response_class(endpoint_description),
        )
        for path, endpoint_description
        in paths_description.items()
    ]


def endpoint_name(endpoint_description: Dict) -> str:
    """
    extract the endpoint name
    """
    return endpoint_description["post"]["operationId"]


def request_class(endpoint_description: Dict) -> str:
    """
    extract the name of the request class
    """
    ref = endpoint_description["post"]["requestBody"]["content"]["application/json"]["schema"]["$ref"]  # noqa: E501  # pylint: disable=line-too-long
    prefix = "#/components/schemas/"
    assert ref.startswith(prefix)
    return ref[len(prefix):]


def response_class(endpoint_description: Dict) -> str:
    """
    extract the name of the response class
    """
    ref = endpoint_description["post"]["responses"]["200"]["content"]["*/*"]["schema"]["$ref"]  # noqa: E501  # pylint: disable=line-too-long
    prefix = "#/components/schemas/"
    assert ref.startswith(prefix)
    return ref[len(prefix):]
