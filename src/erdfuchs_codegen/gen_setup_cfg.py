"""
generate the setup.cfg file
"""


from pathlib import Path
try:
    from importlib.metadata import version  # type:ignore
    # https://github.com/python/mypy/issues/1153
except ImportError:
    from importlib_metadata import version  # type:ignore

from jinja2 import Template


def gen_setup_cfg(output_path: Path) -> None:
    """
    generate the setup.cfg file
    """
    template = Template(
        """
[metadata]
name = erdfuchs-generated
version = {{ version }}
description = API files for geofox GTI api (generated files)
long_description = file: README.md
long_description_content_type = text/markdown;charset=UTF-8;variant=CommonMark
author = Sören Glimm
author_email = git@uncleowen.de
license = Apache
classifiers =
    Programming Language :: Python :: 3
    License :: OSI Approved :: Apache Software License
    Operating System :: OS Independent
    Development Status :: 3 - Alpha

[options]
package_dir =
    =src
packages = find_namespace:
install_requires =
    marshmallow-dataclass

[options.packages.find]
where=src

[options.package_data]
erdfuchs.generated =
    py.typed
""".lstrip(),
        trim_blocks=True,
        keep_trailing_newline=True,
    )

    with open(output_path, "w", encoding="utf-8") as out_f:
        out_f.write(template.render(
            version=version("erdfuchs-codegen"),
        ))
